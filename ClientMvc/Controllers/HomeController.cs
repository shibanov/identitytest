﻿using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ClientMvc.Models;
using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json.Linq;

namespace ClientMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        
        public HomeController(ILogger<HomeController> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public async Task<IActionResult> CallApi()
        {
            var client = _httpClientFactory.CreateClient("UserClient");
            var content = await client.GetStringAsync("identity");

            ViewBag.Json = JArray.Parse(content).ToString();
            return View("Json");
        }
        
        public IActionResult Logout()
        {
            return SignOut("Cookies", "oidc");
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}